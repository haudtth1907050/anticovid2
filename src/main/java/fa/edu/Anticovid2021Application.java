package fa.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntiCovid2021Application {

	public static void main(String[] args) {
		SpringApplication.run(AntiCovid2021Application.class, args);
	}
}
